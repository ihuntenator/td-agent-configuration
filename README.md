## td-agent.conf

Configuration file to use for simple Vagrant self provisioned lab.

## Get the relevent configuration file


```
curl -O https://bitbucket.org/ihuntenator/td-agent-configuration/raw/master/td-agent.lab.conf
curl -O https://bitbucket.org/ihuntenator/td-agent-configuration/raw/master/td-agent.simple.conf
```

### Tags Used

 * monitor.metrics
 * td-agent.log
 * audit.tag


### Testing

For a basic functional test of inputs, filters, and outputs use the bash script evt.sh


## td-agent Event Flows

![Scheme](td-agent-working-audit-color.png)
