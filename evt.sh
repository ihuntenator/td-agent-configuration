#!/usr/bin/bash

fcat=/opt/td-agent/embedded/bin/fluent-cat
audit_message="audit eventstream testing"
secure_message="secure event eventstream testing"
general_message="general eventstream testing"

audit_tag ()
{
  tag="audit.tag"
  echo '{"message":"'"${audit_message}"'","audit_event":"true"}' | ${fcat} ${tag}
  echo '{"message":"'"${audit_message}"'","audit_event":"false"}' | ${fcat} ${tag}
}

audit_grep ()
{
  tag="audit.grep"
  echo '{"message":"'"${audit_message}"'","audit_event":"true"}' | ${fcat} ${tag}
  echo '{"message":"'"${audit_message}"'","audit_event":"false"}' | ${fcat} ${tag}
}

secure_grep ()
{
  tag="secure.grep"
  echo '{"message":"'"${secure_message}"'","security_context":"general"}' | ${fcat} ${tag}
  echo '{"message":"'"${secure_message}"'","security_context":"secure"}' | ${fcat} ${tag}
}

general_message ()
{
  tag="general.message"
  echo '{"message":"'"${general_message}"'","audit_event":"false","security_context":"general"}' | ${fcat} ${tag}
  echo '{"message":"'"${general_message}"'"}' | ${fcat} ${tag}
}

audit_tag
audit_grep
secure_grep
general_message
